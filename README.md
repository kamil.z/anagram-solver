Welcome to Anagram Solver
=======================

**Trustpilot** code challenge accepted ! Behold **Anagram Solver** ! Simple yet powerful class written in **Ruby**, which sole purpose is to encode given anagram and validate it against MD5 digest.

Usage
--------

Class is wrapped in command line executable script. You can run it through terminal:

    $ ruby AnagramSolver.rb

It has predefined *anagram* and *MD5 digest* values. You can edit them directly in script.

Execution Time
----------------------

Script should finish in around *50* seconds for predefined *anagram*.

Roadmap
-------------

Implement Subset Sum algorithm.