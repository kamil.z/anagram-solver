#!/usr/bin/env ruby

require 'digest'

class AnagramSolver
  attr_accessor :anagram, :anagram_chars, :digest, :dictionary, :possible_words, :possible_words_sums, :possible_words_map
  attr_accessor :anagram_sum, :possible_words_sums_combinations, :possible_words_permutations
  
  def initialize(dictionary_path)
    self.dictionary = File.open('en_wordlist.txt', 'r')
  end
  
  # Encode given anagram
  def encode(anagram, digest)
    self.anagram = anagram
    self.digest = digest
    self.anagram_chars = anagram.delete(' ').chars
    self.possible_words_map = {}
    self.anagram_sum = anagram_chars.map(&:ord).reduce(&:+)
    
    self.possible_words = generate_possible_words_list
    self.possible_words_sums = generate_possible_words_list_sums
    self.possible_words_sums_combinations = generate_possible_words_sums_combinations
    self.possible_words_permutations = generate_possible_words_permutations
    
    p "I found: #{find_phrase}"
  end
  
  # Generate list of words which can be created from all characters from anagram
  def generate_possible_words_list
    self.dictionary.select { |word| (word.chop!.chars - anagram_chars).empty? }
  end
  
  # Generate hash map containing words and sum of their bytes
  def generate_possible_words_list_sums
    self.possible_words.map { |word| self.possible_words_map[word] = word.bytes.reduce(&:+) }
  end
  
  # Generate words sums combinations and limit results to combinations for which bytes sum equal to anagram bytes sum
  def generate_possible_words_sums_combinations
    self.possible_words_sums.uniq.combination(self.anagram.scan(/\w+/).size).select { |item| item.reduce(:+) == self.anagram_sum }
  end
  
  # Generate possible phrases
  def generate_possible_words_permutations
    possible_words_permutations = []

    self.possible_words_sums_combinations.each do |combination|
      words = []
      
      # Generate array of words. Each word bytes are summed and compared against bytes sum of combination word.
      combination.each { |word_bytes_sum| words << self.possible_words_map.select { |key, value| value == word_bytes_sum }.keys }
      
      # Generate all possible combinations from array of words
      words.inject(&:product).map(&:flatten).each do |word_combination|
        # Generate permutations for words which total byte value equals to anagram total byte value
        possible_words_permutations.concat word_combination.permutation.to_a.map { |perm| perm.join(' ')}
      end
    end
    
    possible_words_permutations
  end
  
  # Search for phrase which MD5 digest equal to valid digest
  def find_phrase
    self.possible_words_permutations.each { |phrase| return phrase if Digest::MD5.hexdigest(phrase) == self.digest }
  end
end

anagram_solver = AnagramSolver.new('en_wordlist.txt')
anagram_solver.encode('poultry outwits ants', '4624d200580677270a54ccff86b9610e')